jQuery( document ).ready(function() {
    jQuery(window).on("scroll", function() {
        if(jQuery(window).scrollTop() > 10) {
            jQuery(".nav-container-center").addClass("sticky");
        } else {
            //remove the background property so it comes transparent again (defined in your css)
           jQuery(".nav-container-center").removeClass("sticky");
        }
    });
});